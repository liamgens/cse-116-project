package edu.buffalo.cse116.code;

import java.util.ArrayList;
//import java.util.NoSuchElementException;

public class Board {
    /*
    TODO write lots of tests
    TODO add the defaults
    TODO add trap doors
     */

    private ArrayList<Tile> _tiles;
    private ArrayList<Room> _rooms;
    private int _currentRoll;


    public Board(){
        _tiles = generateBoard(25,25);
        _rooms = new ArrayList<Room>();
    }

    public Board(int width, int height){
        _tiles = generateBoard(width,height);
        _rooms = new ArrayList<Room>();
    }

    /**
     * Generates a board of tiles with dimensions width*height
     * @return an ArrayList of Tiles with size width *height
     */
    private ArrayList<Tile>generateBoard(int width, int height){
        ArrayList<Tile> generatedBoard = new ArrayList<Tile>();
        for (int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                generatedBoard.add(new Tile(x, y));
            }
        }
        return generatedBoard;
    }


    public ArrayList<Tile> get_tiles() {
        return _tiles;
    }


    /**
     * The method will add the room entered as an argument to the Board's list of rooms
     * @param room the room object to be added to the list of rooms
     */
    public void addRoom(Room room){
        _rooms.add(room);
    }

    /**
     * The method will iterate through each tile in _tiles and return the number of tiles in a room based on
     * the room that was entered as an argument.
     *
     *
    *@param room number corresponding to the room name
    *@return the number of tiles for the room parameter
     *
    */
    public int roomTileCount(int room) {
        int count = 0;
        for (Tile t : _tiles) {
            if (t.get_parentRoom() == room) {
                count++;
            }
        }
        return count;
    }

    /**
     * Adds the default rooms to the board
     */
    public void addDefaultRooms(){
        this.addRoom(new Room(this.get_tiles(), 0, 0, 6, 5, 6)); //adds the Study
        this.addRoom(new Room(this.get_tiles(), 0, 7, 6, 5, 7)); //adds the Library
        this.addRoom(new Room(this.get_tiles(), 0, 13, 6, 5, 8)); //adds the Billard Room
        this.addRoom(new Room(this.get_tiles(), 0, 20, 6, 5, 2)); //adds the Conservatory
        this.addRoom(new Room(this.get_tiles(), 8, 0, 10, 5, 5)); //adds the Hall
        this.addRoom(new Room(this.get_tiles(), 8, 18, 9, 7, 1)); //adds the Ballroom
        this.addRoom(new Room(this.get_tiles(), 20, 0, 5, 5, 4)); //adds the Lounge
        this.addRoom(new Room(this.get_tiles(), 19, 8, 6, 10, 3)); //adds the Dining Room
        this.addRoom(new Room(this.get_tiles(), 19, 20, 6, 5, 0)); //adds the Kitchen
    }

    /**
     * Adds all of the doors
     */
    public void addDefaultDoors(){
        int[][] doorCoor = {{4,4},{5,9},{3,11},{1,13},{5,16},{5,21},{8,21},{9,18},{15,18},
                {16,21},{20,20},{19,14},{20,8},{21,4},{13,4},{12,4},{8,2}};
        for(int i = 0; i < doorCoor.length; i++){
                getTile(doorCoor[i][0], doorCoor[i][1]).set_isDoor(true);
        }

    }

    /**
     * Mimicks rolling dice
     * @return int value representing the distance a player can move.
     */
    public int rollDice(){
        resetRoll();
        _currentRoll =  (int) (Math.random() * ((6-1) + 1));
        return _currentRoll;
    }

    public void useRoll(){
        if(_currentRoll > 0) {
            _currentRoll --;
        }
    }

    public void resetRoll(){
        _currentRoll = 0;
    }

    public int get_currentRoll(){
        return _currentRoll;
    }


    public Tile getTile(int x, int y){
        for(Tile t : _tiles){
            if(t.get_xCoor() == x && t.get_yCoor() == y){
                return t;
            }
        }
        return null;
    }

    public Room getRoomByID(int iD){
        for(Room r : _rooms){
            if(r.get_idx() == iD){
                return r;
            }
        }return null;
    }


}
