package edu.buffalo.cse116.tests;

import org.junit.Test;
/** Importing Card Class to use instance **/
import edu.buffalo.cse116.code.Card;

import static org.junit.Assert.*;

/**
 * Created by liamgens on 10/9/16.
 *
 * Tests for the Card Class
 * Tests are for individual cards not List
 */

public class CardTest {

	/** Test Card */
	
	private Card _tc = new Card(0, 5);		// !! remove comments when needed !!

	/** Checks if card null, fails if it is */
	@Test
	public void CardNotNullTest() {
		assertNotNull(_tc);
	}

	/** Checks if type is 0, 1, 2; fails if otherwise */
	@Test
	public void TypeOfCardTest() {
		assertTrue("ERROR! Not a card type!", _tc.get_typeOfCard() >= 0 && _tc.get_typeOfCard() <= 2);
		assertFalse("ERROR! Not a card type!", _tc.get_typeOfCard() < 0 || _tc.get_typeOfCard()>2);
	}

	/** Checks if title is empty string "", fails if otherwise */
	@Test
	public void EmptyTitleTest() {
		String stringTest1 = "";
		assertTrue("ERROR! EmptyTitle!", _tc.get_title() != stringTest1);
	}

	/** Checks if title is >= 4 letters and <= 15 letters, fails if not 
	 * There shouldn't be a card title with less than 4 letters, not possible (unless title change though) 
	 * */
	@Test
	public void MoreOrLessLettersTest() {
		String temp = _tc.get_title();
		System.out.println(temp);
		char[] check = temp.toCharArray();
		assertTrue("ERROR! Title is NOT long enough!",check.length >= 4);
		assertTrue("ERROR! Title is TOO long!", check.length <= 15);
	}


}                          
