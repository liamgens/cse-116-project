package edu.buffalo.cse116.tests;

import edu.buffalo.cse116.code.Board;
import edu.buffalo.cse116.code.Room;
import edu.buffalo.cse116.code.Tile;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by liamgens on 10/7/16.
 */
public class RoomTest {

    @Test
    public void roomSize_test(){
        Board b = new Board(24, 24);
        b.addRoom(new Room(b.get_tiles(), 0, 0, 6, 6, 0));
        //check number of tiles for kitchen
        assertEquals(36, b.roomTileCount(0));
//        Checks the x,y coordinates of the tiles in the room
//        int count = 0;
//
//        for(Tile t: b.get_tiles()){
//
//            if(t.get_parentRoom() == 0){
//                System.out.println("x: " + t.get_xCoor() + " y: " + t.get_yCoor());
//                count ++;
//            }
//        }
//
//        System.out.println(count);

    }

    @Test
    public void setDoor_test(){
        Board b = new Board(24, 24);
        Room r = new Room(b.get_tiles(), 0, 0, 6, 6, 0);
        r.addDoors(5, 2);
        r.addDoors(5, 5);
        b.addRoom(r);


        int count = 0;

        for(Tile t: b.get_tiles()){
            if(t.get_isDoor()){
                System.out.println("x: " + t.get_xCoor() + " y: " + t.get_yCoor());
                count++;
            }
        }
        System.out.println(count);


    }

}
