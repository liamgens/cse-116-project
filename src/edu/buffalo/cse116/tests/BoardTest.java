package edu.buffalo.cse116.tests;
import edu.buffalo.cse116.code.Board;
import edu.buffalo.cse116.code.Tile;
import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.*;


public class BoardTest {


	@Test
	public void boardSize_Test(){
		Board b = new Board(24, 24);
		ArrayList<Tile> tiles = b.get_tiles();
		assertEquals(576, tiles.size());

		numOfTiles(tiles);

		b = new Board(0, 0);
		tiles = b.get_tiles();
		assertEquals(0, tiles.size());

		b = new Board(1, 1);
		tiles = b.get_tiles();
		assertEquals(1, tiles.size());
	}
	@Test
	public int numOfTiles(ArrayList<Tile> tiles){
		int count = 0;
		for(Tile t : tiles){
			System.out.println("x: " + t.get_xCoor() + " y: " + t.get_yCoor());
			count++;
		}
		return count;
	}

	/**
	 * Added tests to the Board class on 10/09/2016
	 * Test to make sure that there are six player tokens on the board. 
	 * All of the player tokens should be on the board and none of them should be duplicates. 
	 * We can probably make a token class with _size = 6; and _colors; I'm just throwing stuff in right now.
	 * @andresng
	 */



}
