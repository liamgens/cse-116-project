package edu.buffalo.cse116.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import edu.buffalo.cse116.code.Card;
import edu.buffalo.cse116.code.Deck;

/**
 * Created by liamgens on 10/9/16.
 */


public class DeckTest {
	
	
	/** Checks if dealing deck size is 17 */
	@Test
	public void checkDeckSize() {
		Deck d = new Deck();
		assertTrue(d.size() == 18);
	}
	
	/** Checks if envelope size is 3 */
	@Test
	public void checkEnvelopeSize() {
		Deck d = new Deck();
		ArrayList<Card> num = d.get_envelopeCards();
		assertTrue(num.size() == 3);
	}
	
	/** Checks if envelope cards are of different types*/
	@Test
	public void checkEnvelopeCards() {
		Deck d = new Deck();
		ArrayList<Card> num = d.get_envelopeCards();
		for (Card c : num ) {
			System.out.print(c.get_title() + " ");
		}
		assertTrue(num.get(0).get_typeOfCard() != num.get(1).get_typeOfCard() &&
				   num.get(0).get_typeOfCard() != num.get(2).get_typeOfCard() &&
				   num.get(1).get_typeOfCard() != num.get(2).get_typeOfCard());
		
	}
	
	/** Checks if the envelope cards are of correct type */
	@Test
	public void checkCardsInDeck() {
		Deck d = new Deck();
		ArrayList<Card> p = new ArrayList<Card>();
		ArrayList<Card> w = new ArrayList<Card>();
		ArrayList<Card> r = new ArrayList<Card>();
		for (Card c : d.get_deck()) {
			if (c.get_typeOfCard() == 0) {
				p.add(c);
			} else if (c.get_typeOfCard() == 1) {
				w.add(c);
			} else {
				r.add(c);
			}
		}
		assertTrue(p.size() == 5);
		assertTrue(w.size() == 5);
		assertTrue(r.size() == 8);
	}
}

