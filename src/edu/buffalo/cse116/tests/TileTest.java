package edu.buffalo.cse116.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.buffalo.cse116.code.Tile;

/**
 * Created by liamgens on 10/9/16.
 */
public class TileTest {
	private Tile _tile;
	
	@Test
	public void ValidTileTest(){
		_tile = new Tile(0, 0);
        int x = _tile.get_xCoor();
        int y = _tile.get_yCoor();
		assertTrue(x < 25 && x >=0 && y < 25 && y >= 0);
	}
}
