package edu.buffalo.cse116.tests;

import edu.buffalo.cse116.code.Board;
import edu.buffalo.cse116.code.User;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by liamgens on 10/20/16.
 */
public class MoveLegalityTests {
    @Test
    public void horizontalMoves(){
        Board b = new Board();
        b.addDefaultDoors();
        b.addDefaultRooms();
        User p1 = new User(b, "Mrs. White");

        //checks the starting position
        assertEquals(0, p1.get_posX());
        assertEquals(6, p1.get_posY());
        System.out.println(p1.get_posX() + " " + p1.get_posY());

        assertTrue(p1.makeMove(1,6));
        assertEquals(1, p1.get_posX());
        assertEquals(6, p1.get_posY());
        System.out.println(p1.get_posX() + " " + p1.get_posY());

        assertTrue(p1.makeMove(0,6));
        assertEquals(0, p1.get_posX());
        assertEquals(6, p1.get_posY());
        System.out.println(p1.get_posX() + " " + p1.get_posY());









    }
}
